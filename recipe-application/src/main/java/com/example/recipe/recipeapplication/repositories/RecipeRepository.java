package com.example.recipe.recipeapplication.repositories;

import com.example.recipe.recipeapplication.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, Long>{
}

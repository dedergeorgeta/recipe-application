package com.example.recipe.recipeapplication.repositories;

import com.example.recipe.recipeapplication.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Long> {
}

package com.example.recipe.recipeapplication.controller;

import com.example.recipe.recipeapplication.model.Recipe;
import com.example.recipe.recipeapplication.repositories.RecipeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class RecipeController {

        private RecipeRepository recipeRepository;

        public RecipeController(RecipeRepository recipeRepository) {
            super();
            this.recipeRepository = recipeRepository;
        }

        @GetMapping("/recipes")
        Collection<Recipe> recipes() {
            return recipeRepository.findAll();
        }

        @GetMapping("/recipe/{id}")
        ResponseEntity<?> getRecipe(@PathVariable Long id) {
            Optional<Recipe> recipe = recipeRepository.findById(id);
            return recipe.map(response -> ResponseEntity.ok().body(response))
                    .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }

        @PostMapping("/recipe")
        ResponseEntity<Recipe> createRecipe(@Valid @RequestBody Recipe recipe) throws URISyntaxException {
            Recipe result = recipeRepository.save(recipe);
            return ResponseEntity.created(new URI("/recipe" + result.getId())).body(result);
        }

        @PutMapping("/recipe/{id}")
        ResponseEntity<Recipe> updateRecipe(@Valid @RequestBody Recipe recipe) {
            Recipe result = recipeRepository.save(recipe);
            return ResponseEntity.ok().body(result);
        }

        @DeleteMapping("/recipe/{id}")
        ResponseEntity<?> deleteRecipe(@PathVariable Long id) {
            recipeRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }
}


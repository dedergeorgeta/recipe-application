import React, { Component } from "react";
import AppNav from "../AppNav";
import Footer from "../Footer";
import "../App.css";
import { Link } from "react-router-dom";
import { Container, Button, Form, Input, Label, FormGroup } from "reactstrap";

class Add extends Component {
 
  state = {
    value: {
      id: "104",
      name: "Default Recipe",
      image: "/recipes/recipe-background-alt.jpg",
      category: { id: "1", name: "Vegan" },
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      imageFilePath: null,
      categories: [],
      recipes: [],
      recipe: this.emptyRecipe,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeCategory = this.handleChangeCategory.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let recipe = { ...this.state.recipe };
    recipe[name] = value;
    this.setState({ recipe });
  }

  handleChangeCategory(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let recipe = { ...this.state.recipe };
    recipe[name] = JSON.parse(value);
    this.setState({ recipe });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { recipe } = this.state;
    await fetch("http://localhost:8080/api/recipe", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(recipe),
    });
    console.log(recipe);
  }

  async componentDidMount() {
    const responseCategory = await fetch(
      "http://localhost:8080/api/categories"
    );
    const bodyCategory = await responseCategory.json();
    this.setState({
      categories: bodyCategory,
      isLoading: false,
    });

    const responseRecipe = await fetch("http://localhost:8080/api/recipes");
    const bodyRecipe = await responseRecipe.json();
    this.setState({
      recipes: bodyRecipe,
      isLoading: false,
    });
  }

  render() {
    const title = (
      <h3
        className="pt-2"
        style={{ display: "flex", justifyContent: "center" }}
      >
        Add New Recipe
      </h3>
    );
    const { categories, isLoading } = this.state;

    if (isLoading) {
      return <div>Loading...</div>;
    }

    let optionList = categories.map((category) => (
      <option value={JSON.stringify(category)} id={category.categoryId}> {category.name} </option>
    ));

    return (
      <div className="Site">
        <AppNav />
        <div className="Home-image"></div>
        <Container className="Site-content">
          {title}
          <Form onSubmit={this.handleSubmit}>
            <FormGroup>
              <Label for="name">Title</Label>
              <Input
                type="name"
                name="name"
                id="name"
                onChange={this.handleChange}
                autoComplete="name"
              />
            </FormGroup>

            <FormGroup>
              <Label for="image">ImageUrl </Label>
              <Input
                className="mt-2 ml-2"
                type="name"
                name="image"
                id="image"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <Label for="category">
                Category
              </Label>

              <select
                name="category"
                id="categoryId"
                value={JSON.stringify(this.state.value)}
                onChange={this.handleChangeCategory}
              >
                {optionList}
              </select>
            </FormGroup>

            <FormGroup>
              <Label for="recipeUrl">RecipeUrl</Label>
              <Input
                type="name"
                name="recipeUrl"
                id="recipeUrl"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <Button size="sm" color="primary" type="submit">
                Save Recipe
              </Button>
              <Button
                size="sm"
                className="ml-1"
                color="secondary"
                tag={Link}
                to="/"
              >
                Cancel
              </Button>
            </FormGroup>
          </Form>
        </Container>
        <Footer />
      </div>
    );
  }
}

export default Add;

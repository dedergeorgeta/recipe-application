import React, { Component } from 'react';
import AppNav from '../AppNav';
import Footer from '../Footer';
import {Container} from 'reactstrap';

class ShowRecipe extends Component {
  render() {
    const { data } = this.props.location;
    const title = <h3 className="pt-3" style={{display: 'flex', justifyContent: 'center'}}>{data.name} Recipe</h3>
    const imagePathString = data.image;
    const imageLink = <img className="Recipe-image ml-5 mr-5 mt-2" style={{display: 'flex', justifyContent: 'left'}} src={imagePathString} alt="Test"/>
   const recipePathString = data.recipeUrl;
   const recipeLink =<a href={recipePathString} >Recipe details</a>
    return (
      <div className="Site">
        <AppNav />
        <Container className="Site-content">
          {title}
          <hr  />
          <Container >
            {imageLink}
            {recipeLink}
          </Container>
        
        </Container>
        <Footer />
      </div>
    );
  }
}

export default ShowRecipe;
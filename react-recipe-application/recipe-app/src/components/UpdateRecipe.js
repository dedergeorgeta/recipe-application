  
import React, { Component} from 'react';
import { Container, Button, Form, Input, Label, FormGroup } from "reactstrap";
import AppNav from "../AppNav";
import "../App.css";
import Footer from "../Footer";
import axios from "axios";

class UpdateRecipe extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.id,
      isLoading: true,
     name:"",
     image:"",
     categoryId:"",
     recipeUrl:"",
    };
    this.changeNameHandler = this.changeNameHandler.bind(this);
    this.changeImageHandler = this.changeImageHandler.bind(this);
    this.changeCategoryHandler = this.changeCategoryHandler.bind(this);
    this.updateRecipe = this.updateRecipe.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
 
     axios
       .get("http://localhost:3000/recipe/edit/" + this.props.match.params.id)
       .then((response) => {
         this.setState({
           name: response.data.name,
           image: response.data.image,
           recipeUrl: response.data.recipeUrl,
           categoryId: response.data.categoryId,
         });
       })
       .catch(function (error) {
         console.log(error);
       });
  }
  updateRecipe(newRecipe)  {
  axios.request({
    method: "put",
    url: "http://localhost:3000/recipe/{this.state.id}",
    data: newRecipe,
  })
  .then(response => {
    this.props.history.push("/");
  })
  .catch((err) => console.log(err));
  }
  onSubmit(e) {
  e.preventDefault();
  const newRecipe = {
    name: this.ref.name.value,
    image: this.ref.image.value,
    recipeUrl: this.ref.recipeUrl.value,
    categoryId: this.ref.categoryId.value,
  };
  axios.post("http://localhost:3000/recipe/" + this.props.match.params.id,newRecipe)
  .then(res => console.log(res.data));
  this.updateRecipe(newRecipe);
}
  changeNameHandler = (event) => {
    this.setState({ name: event.target.value });
    
  };

  changeImageHandler = (event) => {
    this.setState({ image: event.target.value });
  };

  changeRecipeHandler = (event) => {
    this.setState({ recipeUrl: event.target.value });
  };

  changeCategoryHandler(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let recipe = { ...this.state.recipe };
    recipe[name] = JSON.parse(value);
    this.setState({ recipe });
  }

  cancel() {
    this.props.history.push("/recipes");
  }

  render() {
    const title = (
      <h3
        className="pt-2"
        style={{ display: "flex", justifyContent: "center" }}
      >
        Update Recipe
      </h3>
    );
    const { categories, isLoading } = this.state;

    if (isLoading) {
      return <div>Loading...</div>;
    }

    let optionList = categories.map((category) => (
      <option value={JSON.stringify(category)} id={category.categoryId}>
        {category.name}
      </option>
    ));

    return (
      <div className="Site">
        <AppNav />
        <div className="Home-image"></div>
        <Container className="Site-content">
          {title}
          <Form onSubmit={this.onSubmit}>
            <FormGroup>
              <Label for="name">Title</Label>
              <Input
                type="name"
                name="name"
                id="name"
                value={this.state.name}
                onChange={this.changeNameHandler}
              />
            </FormGroup>

            <FormGroup>
              <Label for="image">ImageUrl</Label>
              <Input
                className="mt-2 ml-2"
                type="name"
                name="image"
                id="image"
                value={this.state.image}
                onChange={this.changeImageHandler}
              />
            </FormGroup>

            <FormGroup>
              <Label for="category">Category</Label>

              <select
                name="category"
                id="categoryId"
                value={JSON.stringify(this.state.value)}
                onChange={this.changeCategoryHandler}
              >
                {optionList}
              </select>
            </FormGroup>

            <FormGroup>
              <Label for="recipeUrl">RecipeUrl</Label>
              <Input
                type="name"
                name="recipeUrl"
                id="recipeUrl"
                value={this.state.recipeUrl}
                onChange={this.changeRecipeHandler}
              />
            </FormGroup>

            <FormGroup>
              <Button size="sm" color="primary" onClick={this.updateRecipe}>
                Update Recipe
              </Button>
             
              <Button
                size="sm"
                className="ml-1"
                color="secondary"
                onClick={this.cancel.bind(this)}
              >
                Cancel
              </Button>
            </FormGroup>
          </Form>
        </Container>
        <Footer />
      </div>
    );
  }
}

export default UpdateRecipe;

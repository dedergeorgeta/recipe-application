import React, { Component } from 'react';
import AppNav from './AppNav';
import Footer from './Footer';
import './App.css';
import { Link } from 'react-router-dom';
import {Container, ListGroup, ListGroupItem, Button} from 'reactstrap';
// import RecipeService from './services/RecipeService';

class Recipes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      recipes: []
    }
    this.updateRecipe = this.updateRecipe.bind(this);
  }

  async componentDidMount() {
    const responseRecipe = await fetch("http://localhost:8080/api/recipes");
    const bodyRecipe = await responseRecipe.json();
    this.setState({
      recipes: bodyRecipe,
      isLoading: false,
    });
  }

  async remove(id) {
    await fetch("http://localhost:8080/api/recipe/" + id, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then(() => {
      let updatedRecipes = [...this.state.recipes].filter((i) => i.id !== id);
      this.setState({ recipes: updatedRecipes });
    });
  }
  // async edit(id) {
  //   await fetch("http://localhost:8080/api/recipe/" + id, {
  //     method: "UPDATE",
  //     headers: {
  //       Accept: "application/json",
  //       "Content-Type": "application/json",
  //     },
  //   }).then(() => {
  //     let updatedRecipes = [...this.state.recipes].filter((i) => i.id !== id);
  //     this.setState({ recipes: updatedRecipes });
  //   });
  // }
 updateRecipe(id) {
    this.props.history.push('/update/'+ id);
  }
  render() {
    const title = (
      <h3
        className="pt-2"
        style={{ display: "flex", justifyContent: "center" }}
      >
        View All Recipes
      </h3>
    );
    const { recipes, isLoading } = this.state;

    if (isLoading) {
      return <div>Loading...</div>;
    }

    let recipeList = recipes.map((recipe) => (
      <ListGroup key={recipe.id}>
        <ListGroupItem className="mt-1">
          {recipe.name}
          <Button
            size="sm"
            className="float-right ml-1"
            color="danger"
            onClick={() => this.remove(recipe.id)}
          >
            Delete
          </Button>
          <Button
            size="sm"
            className="float-right"
            color="info"
            // tag={Link}
            // to={{
            //   pathname: "/update/:id",
            //   data: {
            //     id: recipe.id,
            //     name: recipe.name,
            //     image: recipe.image,
            //     recipeUrl: recipe.recipeUrl,
            //   },
            // }}
            onClick={() => this.updateRecipe(recipe.id)}
          >
            
            Update
          </Button>
          <Button
            size="sm"
            className="float-right"
            color="info"
            tag={Link}
            to={{
              pathname: "/show",
              data: {
                id: recipe.id,
                name: recipe.name,
                image: recipe.image,
                recipeUrl: recipe.recipeUrl,
              },
            }}
          >
            View Recipe
          </Button>
        </ListGroupItem>
      </ListGroup>
    ));

    return (
      <div className="Site">
        <AppNav />
        <div className="Home-image"></div>
        <Container className="Site-content">
          {title}
          {recipeList}
        </Container>
        <Footer />
      </div>
    );
  }
}

export default Recipes;
// class Recipes extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       isLoading: true,
//       recipes: [],
//     };
//     this.addRecipe = this.addRecipe.bind(this);
//     this.deleteRecipe = this.deleteRecipe.bind(this);

//     this.editRecipe = this.editRecipe.bind(this);
//   }
//   deleteRecipe(id) {
//     RecipeService.deleteRecipe(id).then((res) => {
//       this.setState({
//         recipes: this.state.recipes.filter((recipe) => recipe.id !== id),
//       });
//     });
//   }
//   viewRecipe(id) {
//     this.props.history.push('/recipe/'+id);
//   }
//   editRecipe(id) {
//     this.props.history.push("/recipe/" + id);
//   }
//   componentDidMount() {
//     RecipeService.getRecipe().then((res) => {
//       this.setState({ recipes: res.data });
//     });
//   }
//   addRecipe(id) {
//     this.props.history.push("/recipe/_add");
//   }
//   render() {
//     const title = (
//       <h3
//         className="pt-2"
//         style={{ display: "flex", justifyContent: "center" }}
//       >
//         View All Recipes
//       </h3>
//     );
//     const { recipes, isLoading } = this.state;

//     if (isLoading) {
//       return <div>Loading...</div>;
//     }

//     let recipeList = recipes.map((recipe) => (
//       <ListGroup key={recipe.id}>
//         <ListGroupItem className="mt-1">
//           {recipe.name}
//           <Button
//             size="sm"
//             className="float-right ml-1"
//             color="danger"
//             onClick={() => this.deleteRecipe(recipe.id)}
//           >
//             Delete
//           </Button>
//           <Button
//             size="sm"
//             className="float-right"
//             color="info"
//             // tag={Link}
//             // to={{
//             //   pathname: "/edit/:id",
//             //   data: {
//             //     id: recipe.id,
//             //     name: recipe.name,
//             //     image: recipe.image,
//             //     recipeUrl: recipe.recipeUrl,
//             //   },
//             // }}
//             onClick={() => this.editRecipe(recipe.id)}
//           >
//             Update
//           </Button>
//           <Button
//             size="sm"
//             className="float-right"
//             color="info"
//             tag={Link}
//             to={{
//               pathname: "/show",
//               data: {
//                 id: recipe.id,
//                 name: recipe.name,
//                 image: recipe.image,
//                 recipeUrl: recipe.recipeUrl,
//               },
//             }}
//           >
//             View Recipe
//           </Button>
//         </ListGroupItem>
//       </ListGroup>
//     ));

//     return (
//       <div className="Site">
//         <AppNav />
//         <div className="Home-image"></div>
//         <Container className="Site-content">
//           {title}
//           {recipeList}
//         </Container>
//         <Footer />
//       </div>
//     );
//   }
// }

// export default Recipes;
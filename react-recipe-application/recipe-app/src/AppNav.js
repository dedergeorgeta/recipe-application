import React, { Component } from "react";
import { Nav, Navbar, NavItem,  NavLink } from "reactstrap";

class AppNav extends Component {
  state = {};
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          {/* <NavbarBrand href="/">Recipe App</NavbarBrand> */}
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/add">Add Recipe</NavLink>
            </NavItem>
            
            <NavItem>
              <NavLink href="/recipes">View Recipes</NavLink>
            </NavItem>
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default AppNav;

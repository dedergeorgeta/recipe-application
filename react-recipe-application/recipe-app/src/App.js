import React, { Component } from "react";
import Home from "./Home";
import Add from "./components/Add";
import UpdateRecipe from "./components/UpdateRecipe";
import Recipes from "./Recipes";
import VeganList from "./Categories/Vegan";
import VegetarianList from "./Categories/Vegetarian";
import GlutenFreeList from "./Categories/Gluten";
import FruitList from "./Categories/Fruit";
import GeneralList from "./Categories/General";
import ShowRecipe from "./components/ShowRecipe";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends Component {
  state = {};
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/add" exact={true} component={Add} />
          <Route path="/update/:id"  component={UpdateRecipe} />

          <Route path="/recipes" component={Recipes} />
          <Route path="/show"  component={ShowRecipe} />
          <Route path="/category/vegan" component={VeganList} />
          <Route
            path="/category/vegetarian"
          
            component={VegetarianList}
          />
          <Route
            path="/category/glutenfree"
          
            component={GlutenFreeList}
          />
          <Route path="/category/fruit" exact={true} component={FruitList} />
          <Route
            path="/category/general"
         
            component={GeneralList}
          />
        </Switch>
      </Router>
    );
  }
}

export default App;
